import '../assets/GlobalStyles.module.scss';
import Character from "../components/Character/Character";
import React, {useState, useEffect} from "react";
import {Button, ButtonGroup, Grid, Box} from "@mui/material";
import Typography from "@mui/material/Typography";
import ArrowBackIosNew from '@mui/icons-material/ArrowBackIosNew';
import ArrowForwardIos from '@mui/icons-material/ArrowForwardIos';


function App() {
    const[page, setPage] = useState(1)
    const URL = `https://rickandmortyapi.com/api/character/?page=${page}`;
    const [data, setData] = useState([]);
    useEffect(() => {
        fetch(URL).then((response) => {
            if (response.ok) {
                response.json().then(setData);
            }
        });
    },[URL]);

    return (
        <div style={{padding: "50px"}}>
            <Typography gutterBottom variant="h2" component="div">
                The Rick and Morty API
            </Typography>
            <ButtonGroup style={{margin: "20px 0"}}
                size="small"
                variant="contained">
                <Button
                    disabled={page === 1}
                    onClick={() => setPage(page - 1)}
                    startIcon={<ArrowBackIosNew />}>
                </Button>
                <Typography variant="body2" style={{padding: "10px"}}>
                    {page}
                </Typography>
                <Button
                    disabled={page === 42}
                    onClick={() => setPage(page + 1) }
                    endIcon={<ArrowForwardIos />}>
                </Button>
            </ButtonGroup>
            <Box sx={{ flexGrow: 1 }}>
            <Grid justifyContent="center" container spacing={10}>
                {data.length !== 0 &&
                data.results.map((item) =>
                    (<Grid item>
                        <Character
                            key={item.id}
                            image={item.image}
                            name={item.name}
                            status={item.status}
                            species={item.species}
                            origin={item.origin.name}
                            location={item.location.name}/>

                    </Grid>)
                )
                }
            </Grid>
            </Box>
        </div>
    );
}
export default App;
