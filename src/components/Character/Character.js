import React, {useState} from 'react';
import './Character.scss';
import FavoriteIcon from '@mui/icons-material/Favorite';
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';

import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';

const Character = (props) => {
    const image = props.image;
    const name = props.name;
    const status = props.status;
    const species = props.species;
    const origin = props.origin;
    const location = props.location;

    const [boolFavorite, setBoolFavorite] = useState(false);
    const handleClickFavorite = () => {
        if(boolFavorite){
            setBoolFavorite(false);
        }
        else{
            setBoolFavorite(true);
        }
    }
        return(
            <div className="Character">
                <Card sx={{ minWidth: 345, maxWidth: 345, minHeight: 620}} style={{backgroundColor: "#2b2e3f",  color: "#f3f3f3"}}>
                    <CardMedia
                        component="img"
                        alt="Character image"
                        image={image}
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="div">
                            {name}
                        </Typography>
                        <Typography variant="body2">
                            {status} - {species}
                        </Typography>
                        <Typography variant="body2" style={{color: "#94959b", margin: "5px 0 2px 0"}}>
                            Origin:
                        </Typography>
                        <Typography variant="body2" >
                            {origin}
                        </Typography>
                        <Typography variant="body2" style={{color: "#94959b", margin: "5px 0 2px 0"}}>
                            Last known location:
                        </Typography>
                        <Typography variant="body2">
                            {location}
                        </Typography>
                    </CardContent>
                    <CardActions>
                        {/*<Typography variant="body2">*/}
                        {/*    {boolFavorite ? <FavoriteIcon/> : <FavoriteBorderIcon/>}*/}
                        {/*</Typography>*/}
                        <Button onClick={handleClickFavorite} size="small">{boolFavorite ? <FavoriteIcon/> : <FavoriteBorderIcon/>}</Button>
                        <Typography variant="body2">
                            Add to favorites
                        </Typography>
                    </CardActions>
                </Card>
            </div>
        )

};

export default Character;
